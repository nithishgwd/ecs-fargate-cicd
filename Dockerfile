# Create a minimal image
FROM alpine:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the Go binary from the builder stage
COPY dist/container_metrics/main /app

# Create a directory for metrics files
RUN mkdir -p /app/metrics

# Command to run the executable
CMD ["/app/main"]