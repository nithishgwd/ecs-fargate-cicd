package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
	"io/ioutil"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/mem"
)

const (
	filePath   = "/app/metrics/"
	fileName   = "metrics_"
	fileExt    = ".txt"
	region     = "us-east-1"          // Replace with your AWS region
	bucketName = "testecsbucket"       // Replace with your S3 bucket name
)

func createMetricsFile() (string, error) {
	currentTime := time.Now()
	fileName := fmt.Sprintf("%s%s%s%s", filePath, fileName, currentTime.Format("2006-01-02_15:04:05"), fileExt)

	// Create and write to the metrics file
	data := fmt.Sprintf("Metrics for container:\n%s\n", getContainerMetrics())
	err := ioutil.WriteFile(fileName, []byte(data), 0644)
	if err != nil {
		return "", err
	}

	fmt.Printf("Metrics file created successfully: %s\n", fileName)
	return fileName, nil
}

func getCurrentCPUUsage() (string, error) {
	cpuUsage, err := cpu.Percent(time.Second, false)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%.2f%%", cpuUsage[0]), nil
}

func getCurrentMemoryUsage() (string, error) {
	memory, err := mem.VirtualMemory()
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("Total: %.2f MB, Used: %.2f MB, Free: %.2f MB",
		float64(memory.Total)/1024/1024,
		float64(memory.Used)/1024/1024,
		float64(memory.Free)/1024/1024), nil
}

func getContainerMetrics() string {
	cpuUsage, err := getCurrentCPUUsage()
	if err != nil {
		return fmt.Sprintf("Error getting CPU usage: %v", err)
	}

	memUsage, err := getCurrentMemoryUsage()
	if err != nil {
		return fmt.Sprintf("Error getting memory usage: %v", err)
	}

	return fmt.Sprintf("CPU Usage: %s\nMemory Usage: %s\n", cpuUsage, memUsage)
}

func uploadToS3(localFilePath string) error {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(region),
	})
	if err != nil {
		return err
	}

	s3Client := s3.New(sess)

	file, err := os.Open(localFilePath)
	if err != nil {
		return err
	}
	defer file.Close()

	// Extract file name from path
	fileNameParts := strings.Split(localFilePath, "/")
	objectKey := fileNameParts[len(fileNameParts)-1]

	_, err = s3Client.PutObject(&s3.PutObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
		Body:   file,
	})
	if err != nil {
		return err
	}

	fmt.Printf("File uploaded successfully to S3://%s/%s\n", bucketName, objectKey)
	return nil
}

func main() {
	// Create and write container metrics to file
	filePath, err := createMetricsFile()
	if err != nil {
		fmt.Println("Error creating metrics file:", err)
		os.Exit(1)
	}

	// Upload the metrics file to S3
	err = uploadToS3(filePath)
	if err != nil {
		fmt.Println("Error uploading to S3:", err)
		os.Exit(1)
	}

	// Print a message before exiting
	fmt.Println("Metrics uploaded successfully. Exiting.")

	// Set up a signal handler to capture termination signals
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	// Block until a signal is received
	<-sigCh

	// Perform cleanup or additional logic here if needed

	// Exit the application gracefully
	os.Exit(0)
}